<!DOCTYPE HTML>

<html>
	<head>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />

	</head>
	<body class="landing">

			<header id="header" class="alt skel-layers-fixed">
				<h1>Reversed <span>ZOO</span></h1>  
				<nav id="nav">
					<ul>
						<li><a href="index.php">Home</a></li>	
						<li><a href="authentification2.php">login2</a></li>	
						<li><a href="ticket.php">Ticket</a></li>
						<li><a href="authentification.php">Login</a></li>
					</ul>
				</nav>
			</header>

		  <section id="banner">
				<div class="inner">
					<h2>Reversed Zoo</h2>
					<p style="color:rgb(199, 199, 199); font-size:1.5em";><b>Discover animals you already know</b> </p>
					<ul class="actions">
						<li><a href="#one" class="button">Learn More</a></li>
					</ul>
				</div>
			</section>
			

			<section id="one" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Presentation of our Zoo</h2>
						<p>The main goal of our Zoo is to present animals from everyday's life in our country, for the tourists to discover animals they might not know in their own country, and also for citizens to get new insights of animals they thought they already knew</p>
					</header>
				</div>
				<script src ="function.js"></script>
				<?php
          $heure = date("H");		//mission 3 part - 1
          if ($heure > 12){
            echo "<img src='images/slide01.jpg' class = 'stretch' alt='park'/>";
          }else {
            echo "<img src='images/banner_night.jpg' class = 'stretch' alt='park'/>";
          }
        ?>					
			</section>
			

			<section id="two" class="wrapper style2">
				<div class="container">
					<div class="row uniform">
						<div class="4u 6u(2) 12u$(3)">
							<section class="feature fa-heart">
								<h3>Love and respect</h3>
								<p>Our animals are treated and fed with respect to their needs as individuals. We value happiness and well-being of our animals as much as our workers</p>
							</section>
						</div>
						<div class="4u 6u$(2) 12u$(3)">
							<section class="feature fa-leaf">
								<h3>A green environment</h3>
								<p>We thrive to maintain a big respect to nature and we follow strict ecological rules in order to respect our animals natural habitat and clients comfort</p>
							</section>
						</div>
						<div class="4u$ 6u(2) 12u$(3)">
							<section class="feature fa-map-marker">
								<h3>Location</h3>
								<p>We are Located in France far from any cities nearby in La Creuse, La chappelle-Baloue 23160 to keep in touch with reality and nature</p>
							</section>
						</div>
						<div class="4u 6u$(2) 12u$(3)">
							<section class="feature fa-cutlery">
								<h3>Restoration</h3>
								<p>Our brand new restaurant in the center of our zoo allows you to eat right next to our animals. We also offer a room for a coffe-cat</p>
							</section>
						</div>
						<div class="4u 6u(2) 12u$(3)">
							<section class="feature fa-h-square">	
								<h3>Hotel</h3>
								<p>Since our location is very far from everything, We invite you to sleep in our Hotel right next to our Zoo where comfort and silence are our main focus</p>
							</section>
						</div>
						<div class="4u$ 6u$(2) 12u$(3)">
							<section class="feature fa-money">
								<h3>Payment</h3>
								<p>We accept every form of paiment : cash, check, debit card, credit card, bitcoin, dogecoin, V-bucks etc.. as long as we get our money</p>
							</section>
						</div>
					</div>
				</div>
			</section>
			

			<section id="three" class="wrapper style1">
				<div class="container">
					<header class="major">
						<h2>Our main services</h2>
						<p>We propose you a wide range of choice to discover our Zoo. You will come across dangerous predators as white wolves but also sweet beauties like sausage dog or persian cat</p>
					</header>
					<div class="row">
						<div class="4u 6u(2) 12u$(3)">
							<article class="box post">
								<a href="#" class="image fit"><img src="images/pic01.jpg" alt="" /></a>
								<h3>Happy cats</h3>
								<p>Discover our wonderful cats in our restaurant mixed with wonderful felines. (button to AfficheListeTickets.php)</p>
								<ul class="actions">
									<li><a href="afficheListeTickets.php" class="button">display all</a></li>
								</ul>
							</article>
						</div>
						<div class="4u 6u$(2) 12u$(3)">
							<article class="box post">
								<a href="#" class="image fit"><img src="images/pic02.jpg" alt="" /></a>
								<h3>Happy sausage dogs</h3>
								<p>Our lovely dogs will bring you joy and excitement during your journey in our zoo. (button to AfficherTicket.php)</p>
								<ul class="actions">
									<li><a href="afficherTicket.php" class="button">Display 1 Ticket</a></li>
								</ul>
							</article>
						</div>
						<div class="4u$ 6u(2) 12u$(3)">
							<article class="box post">
								<a href="#" class="image fit"><img src="images/pic03.jpg" alt="" /></a>
								<h3>White wolves</h3>
								<p>Take a look at our terrifying predators in their almost natural habitat!</p>
								<ul class="actions">
									<li><a href="#" class="button">Learn More</a></li>
								</ul>
							</article>
						</div>
						<div class="4u 6u$(2) 12u$(3)">
							<article class="box post">
								<a href="#" class="image fit"><img src="images/pic04.jpg" alt="" /></a>
									<h3>Disgusting cockroaches</h3>
									<p>These particular typical insect from France will revolt you from beeing dirty.</p>
									<ul class="actions">
										<li><a href="#" class="button">Learn More</a></li>
									</ul>
							</article>
						</div>
						<div class="4u 6u(2) 12u$(3)">
							<article class="box post">
								<a href="#" class="image fit"><img src="images/pic05.jpg" alt="" /></a>
								<h3>Angry boars</h3>
								<p>Angry boars might be one of the highest ratio of "dangerous/common" animal in france. Their impressive tusks might surprise you.</p>
								<ul class="actions">
									<li><a href="#" class="button">Learn More</a></li>
								</ul>
							</article>
						</div>
						<div class="4u$ 6u$(2) 12u$(3)">
							<article class="box post">
								<a href="#" class="image fit"><img src="images/pic06.jpg" alt="" /></a>
								<h3>Sweet deer</h3>
								<p>The innocence of this animal might melt your heart so fast that you will cry all your tears watching Bambi.</p>
								<ul class="actions">
									<li><a href="#" class="button">Learn More</a></li>
								</ul>
							</article>
						</div>
					</div>
				</div>
			</section>
			
			<section id="cta" class="wrapper style3">
				<h2>Get your reservation now (and inscription)</h2>
				<ul class="actions">
					<li><a href="inscription.php" class="button big">Inscription</a></li>
				</ul>
			</section>
			<footer> 
				<a href = "https://entreprendre.service-public.fr/vosdroits/F31228">Legal notices</a>
				<a href = "https://entreprendre.service-public.fr/vosdroits/F24270">RGPD</a>
				<a href = "https://www.cnil.fr/fr/cookies-et-autres-traceurs">regulation around Cookies</a>
				<a href = "https://www.cnil.fr/fr/cookies-et-autres-traceurs">CCPA california</a>
				<a href = "https://www.cookiebot.com/fr/what-is-ccpa/">CCPA france</a>
			</footer>
	</body>
</html>