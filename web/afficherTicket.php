<!DOCTYPE html> <!-- mission 4 part 3 -->
<html>
<head>
      <meta charset="utf-8">
      <title>Tickets</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />

	</head>
	<body>
		<header id="header" class="alt skel-layers-fixed">
			<h1>Reversed <span>ZOO</span></h1>
			<nav id="nav">
					<ul>
						<li><a href="index.php">Home</a></li>	
						<li><a href="authentification2.php">login2</a></li>	
						<li><a href="ticket.php">Ticket</a></li>
						<li><a href="authentification.php">Login</a></li>
					</ul>
				</nav>
		</header>
        <br>
        <br>

<div class="container">
  <h4>choose a ticket to show by selecting its ID </h4>
  <form action="" method="post" >
    <div class="form-group">
      <input type="text" class="form-control" name="id" placeholder="Enter id" required>
    </div>
    <button type="submit" class="btn btn-default" name="submit">Submit</button>
  </form>
</div>



<?php

if ( isset( $_POST['submit'] )) {   // we take the submit value of the button in the form ticket
    $id = $_POST['id']; 

    $mysqli = new mysqli("localhost", "root", "", "bdd_zoo");
    
    if($mysqli === false){
        die("ERROR: Could not connect. " . $mysqli->connect_error);
    }

    $sql = "SELECT * FROM ticket WHERE `id` = '$id'";

    if($result = $mysqli->query($sql)){     //using the same code as afficheListeTicket.php, but only whith its mathcing ID

        if($result->num_rows > 0){

                echo "<table class='table'>";
                    echo "<tr>";
                        echo "<th>id</th>";        // print title column , and while there are rows, print those values
                        echo "<th>datet</th>";
                        echo "<th>login</th>";
                        echo "<th>sujet</th>";
                        echo "<th>description</th>";
                        echo "<th>prio</th>";
                        echo "<th>secteur</th>";
                        echo "<th>statut</th>";
                    echo "</tr>";
                while($row = $result->fetch_array()){
                    echo "<tr>";
                        echo "<td>" . $row['id'] . "</td>";
                        echo "<td>" . $row['datet'] . "</td>";
                        echo "<td>" . $row['login'] . "</td>";
                        echo "<td>" . $row['sujet'] . "</td>";
                        echo "<td>" . $row['description'] . "</td>";
                        echo "<td>" . $row['prio'] . "</td>";
                        echo "<td>" . $row['secteur'] . "</td>";
                        echo "<td>" . $row['statut'] . "</td>";
                    echo "</tr>";
                }
                echo "</table>";

            $result->free();
            ?>
           <div class='container'>   
            <h4> re-type the id to change its status to 'Resolu' </h4>  <!-- we're only showing the second form if the query acces the database -->
            <form action='modifierTicket.php' method='post'>
                <input type='text' class='form-control' name='id' placeholder='$id' required>
                <button type='submit' class='btn btn-default' name='submit2'>submit</button>
            </form>
        </div>
        <?php
        } else{
            echo "No data matching your query was found in the database.";
        }
    } else{
        echo "ERROR:not able to execute $sql. " . $mysqli->error;
    }  
    $mysqli->close();
}
?>
           </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</body>
</html>