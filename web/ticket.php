<!DOCTYPE HTML> <!-- mission 2 ticket -->

<html>
	<head>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<title>Ticket</title>
	</head>
	<body>
		<header id="header" class="alt skel-layers-fixed">
			<h1>Reversed <span>ZOO</span></h1>
			<nav id="nav">
					<ul>
						<li><a href="index.php">Home</a></li>	
						<li><a href="authentification2.php">login2</a></li>	
						<li><a href="ticket.php">Ticket</a></li>
						<li><a href="authentification.php">Login</a></li>
					</ul>
				</nav>
		</header>

			<section id="main" class="wrapper style1">
				<header class="major">
					<h2>Ticket</h2>
					<p>You're here to fill a ticket : Follow these steps to finish your complain</p>
				</header>
				<div class="container">						
						<section id="content">
								<section>
									<h3>Form Ticket</h3>
									<form method="post" action="recupTicket.php"> <!-- mission 4 part 1 + montrer phpmyadmin-->
										<div class="row uniform">
											<div class="6u 12u(3)">
												<input type="email" name="email" id="email" value="" placeholder="Email" />
											</div>
										</div>
										<div class="row uniform">
										

										<div class="12u">
											<input type="text" name="message" id="message" placeholder="Subject"/>
										</div>
									</div>
										
										<div class="row uniform">
											<div class="12u">
												<div class="select-wrapper">
													<select name="secteur" id="secteur">
														<option value="Aucun">- Secteur -</option>
														<option value="Savane">Savane zone</option>
														<option value="Domestic">Domestic zone</option>
														<option value="Restaurant">Restaurant zone</option>
														<option value="Angry Animals">Angry Animals zone</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row uniform">
											<div class="12u">
												<div class="select-wrapper">
													<select name="urgency" id="urgency">
														<option value="No data">- select your urgency -</option>
														<option value="faible">it's fine</option>
														<option value="moyen">a little urgent</option>
														<option value="fort">urgent</option>
														<option value="très fort">VERY URGENT</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row uniform">
										</div>
										<div class="row uniform">
											<div class="12u">
												<input type ="text" name="description" id="description" placeholder="Enter your message" rows="6"/>
											</div>
										</div>
										<div class="row uniform">
											<div class="12u">
												<ul class="actions">
													<li><button type="submit" class = "btn btn-primary" value="Submit" name="submit" ></li>
													<li><input type="reset" value="Reset" class="alt" /></li>
												</ul>
											</div>
										</div>
									</form>
									<div class="Zoom">
										<img src="images/complain.jpg" alt="Complain">
									</div>
								</section>
	<!-- mission 2 part 1 --><style> 
	
	.Zoom {
        width: 20%;
        height: 20%;
        margin: 0 auto;
    }
       
    .Zoom img {
        width: 100%;
        transition: 0.5s all ease-in-out;
    }
       
    .Zoom:hover img {
        transform: scale(1.5);
    }
							
							
							</style>
						</section>
				</div>
			</section>
	</body>
</html>