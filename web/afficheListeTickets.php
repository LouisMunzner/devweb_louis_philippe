<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>Tickets</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />

	</head>
	<body>
		<header id="header" class="alt skel-layers-fixed">
			<h1>Reversed <span>ZOO</span></h1>
			<nav id="nav">
					<ul>
						<li><a href="index.php">Home</a></li>	
						<li><a href="authentification2.php">login2</a></li>	
						<li><a href="ticket.php">Ticket</a></li>
						<li><a href="authentification.php">Login</a></li>
					</ul>
				</nav>
		</header>
        <br>
        <br>
      <div class="container">
         <h2>Tickets</h2>

<?php //mission 4 part 2

$mysqli = new mysqli("localhost", "root", "", "bdd_zoo");   //connection to database
 

if($mysqli === false){
    die("ERROR: Could not connect. " . $mysqli->connect_error);
}
 
$sql = "SELECT * FROM ticket";      // we take everything to display it later
if($result = $mysqli->query($sql)){
    if($result->num_rows > 0){
        echo "<table class='table'>";
            echo "<tr>";
                echo "<th>id</th>";        // print title column , and while there are rows, print those values
                echo "<th>datet</th>";
                echo "<th>login</th>";
                echo "<th>sujet</th>";
                echo "<th>description</th>";
                echo "<th>prio</th>";
                echo "<th>secteur</th>";
                echo "<th>statut</th>";
            echo "</tr>";
        while($row = $result->fetch_array()){
            echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['datet'] . "</td>";
                echo "<td>" . $row['login'] . "</td>";
                echo "<td>" . $row['sujet'] . "</td>";
                echo "<td>" . $row['description'] . "</td>";
                echo "<td>" . $row['prio'] . "</td>";
                echo "<td>" . $row['secteur'] . "</td>";
                echo "<td>" . $row['statut'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        $result->free();
    } else{
        echo "No records matching your query were found.";
    }
} else{
    echo "ERROR: Not able to execute $sql. " . $mysqli->error;
}
$mysqli->close();
?>
           </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</body>
</html>